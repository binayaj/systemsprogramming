#!/usr/bin/env python
import os,sys,time,subprocess

#Make sure that correct number of arguments were passwed and the argument is a directory
if (len (sys.argv) == 2):
	if (os.path.isdir(sys.argv[1])):
		#Generate unique filenames using EPOCH time and pid so that it doesn't cause conflict even if it is run concurrently
		rand_str=(str(time.time()).replace(".",""))+str(os.getpid())
		errorfile="error_report"+rand_str+".txt"
		tempfile="tempfile"+rand_str+".tmp"
		with open (tempfile,'w') as fp,  open(errorfile, 'w') as fp1:
			# Traverse recursively through the directory.
			for dir, subdir, files in os.walk(sys.argv[1]):
				for file in files:
					path = os.path.join(dir, file)
					try:
                                          # Save the size of all files to a remporary file and exclude symbolic links in it.
						if (not os.path.islink(path)):
							size = os.stat(path).st_size
							fp.write ("{0}" " {1}\n".format (size,path))
					# Catch any possible errors
					except OSError as e: 
						fp1.write("Error encountered while accessing {}\n".format(path))
		#Closing temporary files
		fp1.close()
		fp.close()		   
		#sort the contents of files based on their size 
		p1 = subprocess.Popen(('sort', '-nr',tempfile), stdout=subprocess.PIPE)
		# Pipe results and print formatted output to screen 
		p2 = subprocess.Popen (('awk', 'BEGIN {print \"{\\n \\"files\\":\\n\\t[\"} {if (1<NR) print line ",";line= \"\\t{\\"\"$2"\\":\" $1\"}"} END {print line"\\n\t]\t\\n}"}' ),stdin=p1.stdout)
		p2.wait()
		# Delete the temporary file.
		os.remove (tempfile)
	else:
		print ("Directory doesn't exist")
		exit(1)
else: 
	print("Usage: sript directory")

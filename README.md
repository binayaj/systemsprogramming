# PROJECT TITLE:
Systems Programming

##PREREQUISITES:
Python 2.7

##SUMMARY:
The program gets sizes of all files of a folder/mount point in non-increasing order in json format. It excludes any symbolic links that are encourtered during the process.


##INSTALLATION:
Download the source code by cloning the repository as below:
```
git clone https://binayaj@bitbucket.org/binayaj/systemsprogramming.git
cd systemsprogramming
su
chmod 755 diskusage.py cleanup.sh

```

##USAGE:
To find sizes of all files of a particular folder/mount point, please run the command as below:
```
./diskusage.py <mount point>

Eg:
./diskusage.py /usr/bin

```
##RESULTS:
The results of the program are displayed on the screen. It can also be saved to a file as below:
```
./diskusage.py /usr/bin > sample_result.json

```
A sample result run on /usr/bin has been saved to a file named 'sample_result.json'

##NOTE:
* In the program, json.dump module has not been used on purpose to make sorting quick and easy.

* Also, during program execution if it encounters any errors while accessing files, it is logged to a file named error_report[0-9]*.txt.
Please run following command to clean up those files later.
```
./cleanup.sh

```

